1) To enable on Apache simply add it to your httpd.conf file (Apache config file). Just add at the most bottom:
Header always set x-frame-options "SAMEORIGIN"
Header always set X-Xss-Protection "1; mode=block"
Header always set X-Content-Type-Options "nosniff"
Header always set Content-Security-Policy "default-src https: data: 'unsafe-inline' 'unsafe-eval'" --> comment: remove for the time being cuz it messed up the website