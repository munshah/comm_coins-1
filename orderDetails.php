<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Order Details</title>
	<!-- Custom CSS -->
	<link rel ="stylesheet" type="text/css" href="css/main.css">
	<script type="text/javascript">
		function printPage() {
			window.print();
		}
	</script>
	<style id="antiClickjack">body{display:none !important;}</style>
	<script type="text/javascript">
		if (self === top) {
			var antiClickjack = document.getElementById("antiClickjack");
			antiClickjack.parentNode.removeChild(antiClickjack);
		} else {
			top.location = self.location;
		}
	</script>
</head>

<body>  
	<form id="details-form" name="details-form" method="post" action="">
		<div align="center">
			<p>&nbsp;</p>
			<table width="1045" border="1">
				<tr>
					<td width="1035" height="64" bgcolor="#000066"><span class="a">BANK NEGARA MALAYSIA - Commemorative Coin Order Details</span></td>
				</tr>
				<tr>
					<td height="118" bgcolor="#FFFFFF"><table width="928" border="0">
						<tr>
							<td width="922" height="314"><div align="center">
							</div>
							<table width="903" border="0">
								<tr>
									<td width="161">&nbsp;</td>
									<td><p><span class="c"><b>
										<?php 
										session_start();
										echo 'Name:</b></span><b> </b> <span class="c">' . $_SESSION['fullname'] . '<br></span> </p>';
										echo '<p> <span class="c"><b>MyKad/Passport: </b>' . $_SESSION['mykad'] . '<br>';

										if($_SESSION['silverqty'] != "0") {
											$coin_type = "Silver Coin - " . $_SESSION['silverqty'];
										}

										if($_SESSION['goldQty'] != "0" && !empty($coin_type)) {
											$coin_type .= ", Gold Coin - " . $_SESSION['goldQty'];
										} else if($_SESSION['goldQty'] != "0" && empty($coin_type)){
											$coin_type = "Gold Coin - " . $_SESSION['goldQty'];
										}

										if($_SESSION['setqty'] != "0" && !empty($coin_type)) {
											$coin_type .= ", Set of two - " . $_SESSION['setqty'];
										} else if($_SESSION['setqty'] != "0" && empty($coin_type)){
											$coin_type = "Set of two - " . $_SESSION['setqty'];
										}	
										echo ' </span>
										<p> <span class="c"><b>Order Details: </b>' . $coin_type . '<br>';
											echo '</span>
											<p> <span class="c"><b>Collection Point: </b>' . $_SESSION['collect_pt'] . '</span><br>';
												echo '<p class="c"> <b>Order Date: </b>' . $_SESSION['order_dt'] . '</td>';
												?>
											</tr>
											<tr>
												<button class="submit-btn" onclick="printPage()">Print this page</button>
											</tr>
										</table>
										<p>&nbsp;</p></td>
									</tr>

								</table></td>
							</tr>

						</table>
					</div>
					<p>&nbsp;</p>
					<p>&nbsp;</p>
				</form>


			</body>

			</html>


