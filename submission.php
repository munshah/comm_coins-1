<?php

session_start();

//report any error
error_reporting(E_ALL); ini_set('display_errors', 1); mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

//connect to db
include 'db_connect.php';

//recaptcha verification
if (isset($_POST["g-recaptcha-response"])) {

	$response = $_POST["g-recaptcha-response"];
	$url = 'https://www.google.com/recaptcha/api/siteverify';
	$data = array(
		'secret' => '6LdmfikUAAAAALCSClhw0yIuB1K0MhtijMZv217s', //test: 6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe
		'response' => $_POST["g-recaptcha-response"]
		);
	$options = array(
		'http' => array (
			'header' => "Content-type: application/x-www-form-urlencoded\r\n",
			'method' => 'POST',
			'content' => http_build_query($data)
			)
		);
	$context  = stream_context_create($options);
	$verify = file_get_contents($url, false, $context);
	$captcha_success=json_decode($verify);

	if ($captcha_success->success==false) {
		//echo "<p>You are a bot! Go away!</p>";
		echo "<script type='text/javascript'>";
		echo "alert('To continue, please make sure you are not a robot.');";
		echo "window.location.href='userinfo.php';";
		echo "</script>";
	} else if ($captcha_success->success==true) {
		echo "<p>You are not a bot!</p>";

		//submit form
		if(isset($_POST['submit'])) {


			//php validation if html validation failed
			//check if variable is null
			if(!isset($_SESSION['consent-rd']) || !isset($_SESSION['Rddcollection']) || !isset($_POST['fullname']) || !isset($_POST['mykad']) || !isset($_POST['phone_num']) || !isset($_POST['email']) || (($_SESSION['goldQty']=='0' && $_SESSION['silverqty']=='0') && $_SESSION['setqty']=='0') ) {
				//terminate submission
				echo "Please enter the required fields.";
				header("Refresh: 3; url= index.php");

			//check if variable is empty string, false, array(), NULL, unset var, 0, "0"
			} else if(empty($_SESSION['consent-rd']) || empty($_POST['fullname']) || empty($_POST['mykad']) || empty($_POST['phone_num']) || empty($_POST['email']) ) {
				echo "Please enter the required fields.";
				header("Refresh: 3; url= index.php");
			} else {
				$fullname = stripslashes($_POST['fullname']);
				$fullName = strtoupper($fullname);
				$fullName = trim($fullName);

				if(!isValidName($fullName)) {
					echo "Please enter a valid name.";
					header("Refresh: 3; url= index.php");
				}

				$mykad = stripslashes($_POST['mykad']);
				$mykad = strtoupper(mysqli_real_escape_string($conn, $mykad));
				$mykad = trim($mykad);

				if(!isValidMykad($mykad)) {
					echo "Please enter a valid mykad or passport number without dash.";
					header("Refresh: 3; url= index.php");
				}

				$phone_num = stripslashes($_POST['phone_num']);
				$phone_num = mysqli_real_escape_string($conn, $phone_num);
				$phone_num = trim($phone_num);
				
				if(!isValidPhone($phone_num)) {
					echo "Your phone number should be between 9 - 10 digits including area codes and without dash.";
					header("Refresh: 3; url= index.php");
				}

				$email = stripslashes($_POST['email']);
				$email = mysqli_real_escape_string($conn, $email);
				$email = trim($email);

				if(!isValidEmail($email)) {
					echo "Your email is invalid";
					header("Refresh: 3; url= index.php");
				}

				$collect_pt = $_SESSION['Rddcollection'];
				$collect_pt = stripslashes($collect_pt);
				$collect_pt = trim($collect_pt);
				$collect_pt = mysqli_real_escape_string($conn, $collect_pt);

				if(!isValidChoice($collect_pt)) {
					echo "Your must choose a collection point.";
					header("Refresh: 3; url= index.php");
				} else {
					switch($collect_pt) {
						case 0:
						$collect_pt = "BNM HQ - KL";
						break;

						case 1:
						$collect_pt = "BNM Pulau Pinang";
						break;

						case 2:
						$collect_pt = "BNM Kuala Terengganu";
						break;

						case 3:
						$collect_pt = "BNM Johor Bahru";
						break;

						case 4:
						$collect_pt = "BNM Kota Kinabalu";
						break;

						case 5:
						$collect_pt = "BNM Kuching";
						break;
					}
				}

				date_default_timezone_set('Asia/Kuala_Lumpur');
				$order_dt = date('d/m/Y h:i:s a', time()); //TODO: remote mysql order_dt is different than time zone order dt


				$consent = $_SESSION['consent-rd'];
				$consent = trim($consent);
				$consent = mysqli_real_escape_string($conn, $consent);

				if($consent == "agree") {
					$pdpa_consent = "Y";
				} else {
					$pdpa_consent = "N";
					if($pdpa_consent == "N") {
						echo "Please accept the terms and conditions.";
						header("Refresh: 3; url= index.php");
					}
				}

				$gold_coin = $_SESSION['goldQty'];
				$gold_coin = trim($gold_coin);
				$gold_coin = mysqli_real_escape_string($conn, $gold_coin);
				if(!isValidChoice($gold_coin)) {
					echo "Your order is invalid."; 
					header("Refresh: 3; url= index.php");
				}

				$silver_coin = $_SESSION['silverqty'];
				$silver_coin = trim($silver_coin);
				$silver_coin = mysqli_real_escape_string($conn, $silver_coin);
				if(!isValidChoice($silver_coin)) {
					echo "Your order is invalid."; 
					header("Refresh: 3; url= index.php");
				}

				$set_coin = $_SESSION['setqty'];
				$set_coin = trim($set_coin);
				$set_coin = mysqli_real_escape_string($conn, $set_coin);
				if(!isValidChoice($set_coin)) {
					echo "Your order is invalid."; 
					header("Refresh: 3; url= index.php");
				}

				//create session
				$_SESSION['fullname'] = $fullName;
				$_SESSION['mykad'] = $mykad;
				$_SESSION['phone_num'] = $phone_num;
				$_SESSION['email'] = $email;
				$_SESSION['order_dt'] = $order_dt;
				$_SESSION['collect_pt'] = $collect_pt;

				//insert order details to DB
				$insertToDB = "INSERT INTO `coinorder` (fullname, mykad, phone_num, email, gold_coin, silver_coin, set_coin, collect_pt, pdpa_consent)
				VALUES (?,?,?,?,?,?,?,?,?)";

				if($statementIns = $conn->prepare($insertToDB)) {
					$statementIns->bind_param('ssisiiiss',$fullName, $mykad, $phone_num, $email, $gold_coin, $silver_coin, $set_coin, $collect_pt, $pdpa_consent);
					$statementIns->execute();
					$statementIns->close();
					echo "Submission is successful";
					header("Refresh: 1; url= orderDetails.php"); //TODO: generate order details to be print out or save
				} else {
					echo "Unsuccessful submission. Please try again.";
					header("Refresh: 1; url= index.php");
				}

	} //if all fields are filled in

} //if submit
$conn->close();
}
} else { //if human
	die('Please make sure you are human!');
}

function isValidName($name) {
	return preg_match('/^[a-zA-Z\'\-\040]+$/',$name);
}

function isValidMykad($mykad) {
	return preg_match('/^[a-zA-Z0-9]+$/',$mykad);
}

function isValidEmail($email) {
	return filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match ('/@.+\./', $email);
}

function isValidPhone($phone) {
	return preg_match('/^[0-9]{9,10}$/', $phone);
}

function isValidChoice($choice) {
	return preg_match('/^[0-5]{1}$/', $choice);
}

?>